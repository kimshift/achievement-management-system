/*
 Navicat Premium Data Transfer

 Source Server         : MongoDB
 Source Server Type    : MongoDB
 Source Server Version : 30404
 Source Host           : localhost:27017
 Source Schema         : student_score

 Target Server Type    : MongoDB
 Target Server Version : 30404
 File Encoding         : 65001

 Date: 23/10/2020 17:37:18
*/


// ----------------------------
// Collection structure for scores
// ----------------------------
db.getCollection("scores").drop();
db.createCollection("scores");

// ----------------------------
// Documents of scores
// ----------------------------
db.getCollection("scores").insert([ {
    _id: ObjectId("5f928f953c8a4637844c3e0d"),
    Sno: NumberInt("1183383"),
    Sname: "方婷宜",
    "machine_learning": NumberInt("98"),
    nodejs: NumberInt("98"),
    microservice: NumberInt("98"),
    rstudio: NumberInt("98"),
    "system_safety": NumberInt("98"),
    __v: NumberInt("0")
} ]);
db.getCollection("scores").insert([ {
    _id: ObjectId("5f928fa43c8a4637844c3e0e"),
    Sno: NumberInt("1008915"),
    Sname: "小陈",
    "machine_learning": NumberInt("100"),
    nodejs: NumberInt("100"),
    microservice: NumberInt("100"),
    rstudio: NumberInt("100"),
    "system_safety": NumberInt("100"),
    __v: NumberInt("0")
} ]);
db.getCollection("scores").insert([ {
    _id: ObjectId("5f928fa83c8a4637844c3e0f"),
    Sno: NumberInt("1034277"),
    Sname: "方廷轩",
    "machine_learning": NumberInt("99"),
    nodejs: NumberInt("99"),
    microservice: NumberInt("99"),
    rstudio: NumberInt("99"),
    "system_safety": NumberInt("99"),
    __v: NumberInt("0")
} ]);
db.getCollection("scores").insert([ {
    _id: ObjectId("5f928fac3c8a4637844c3e10"),
    Sno: NumberInt("1953736"),
    Sname: "琴时明月",
    "machine_learning": NumberInt("100"),
    nodejs: NumberInt("100"),
    microservice: NumberInt("100"),
    rstudio: NumberInt("100"),
    "system_safety": NumberInt("100"),
    __v: NumberInt("0")
} ]);
db.getCollection("scores").insert([ {
    _id: ObjectId("5f928faf3c8a4637844c3e11"),
    Sno: NumberInt("1296637"),
    Sname: "琴时",
    "machine_learning": NumberInt("97"),
    nodejs: NumberInt("97"),
    microservice: NumberInt("97"),
    rstudio: NumberInt("97"),
    "system_safety": NumberInt("97"),
    __v: NumberInt("0")
} ]);
db.getCollection("scores").insert([ {
    _id: ObjectId("5f928fb33c8a4637844c3e12"),
    Sno: NumberInt("1345309"),
    Sname: "明月",
    "machine_learning": NumberInt("96"),
    nodejs: NumberInt("96"),
    microservice: NumberInt("96"),
    rstudio: NumberInt("96"),
    "system_safety": NumberInt("96"),
    __v: NumberInt("0")
} ]);
