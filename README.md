### 项目概述

- 项目名称：学生成绩管理系统
- 开发时间：2020.04~2020.06
- 开发涉及：Node、Vue、MongoDB、Bootstrap、vscode 开发工具
- 项目描述：通过 nodejs+mongodb 完成一个学生成绩管理系统：1.添加功能 2.修改功能 3.删除功能 4.精确查询功能 5.模糊查询功能 6.数据分页
- 我的职责：利用 node 实现全栈开发

### 运行项目
- 视频第二集是运行教程可自行观看：https://www.bilibili.com/video/BV1Cf4y1m7oh/
- 编译方式可自行选择：vscode编译器、cmd窗口
- 数据库文件：student_score.sql 可自行导入本地数据库
- 安装依赖：npm install
- 运行服务：npm start

### 功能展示

- 首页界面<br />
  ![首页界面](img/index.png)<br />
- 添加功能界面<br />
  ![新增界面](img/create.png)<br />
- 添加成功界面<br />
  ![成功界面](img/create-success.png)<br />
- 添加失败界面<br />
  ![失败界面](img/create-fail.png)<br />
- 修改功能界面<br />
  ![修改界面](img/edit.png)<br />
- 修改成功界面<br />
  ![成功界面](img/edit-success.png)<br />
- 修改失败界面<br />
  ![失败界面](img/edit-fail.png)<br />
- 删除功能界面<br />
  ![删除界面](img/delete.png)<br />
- 删除成功界面<br />
  ![成功界面](img/delete-success.png)<br />
- 精确查询功能<br />
  ![查询界面](img/query.png)<br />
- 模糊查询功能<br />
  ![查询界面](img/querys.png)<br />
